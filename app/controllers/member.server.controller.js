/**
 * Created by Bitcamp on 2016-06-09.
 */

'use strict'

var Member=require('../models/member.server.model.js');

exports.list=function(req,res,next){
    var select= {
        userId:1,
        userPwd:1,
        name:1,
        userEmail:1,
        regData:1
    }
    var where={};
    Member.find(where,select).exec(function(err,result){
        if(err){
            err.status=500;
            next(err);
        }else{
            res.jsonp({result:result});
        }
    });
}

exports.add=function(req,res,next) {
    var member=new Member();
    console.log(req.body)
    member.userId=req.body.userId;
    member.userPwd=req.body.userPwd;
    member.name=req.body.name;
    member.userEmail=req.body.userEmail;


    member.save(function(err,result){
        if(err){
            err.status=500;
            next(err);
        }else{
            res.jsonp({result:'success'});//인서트가 완료가 되면 장상적인 신호를 보내준다.
        }
    });
}