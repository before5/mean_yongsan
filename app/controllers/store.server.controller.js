/**
 * Created by Bitcamp on 2016-06-02.
 */

'use strict'

//사용할 모듈을 임포트한다.
var Store = require('../models/store.server.model');

//스토어의 목록을 가지고온다.
exports.list=function(req, res, next){
    var select={
        store_name_kr:1,
        store_name_en:1,
        address:1,
        categori:1,
        location:1,
        regNum:1,
        menus:1
    };
    var where={};
    Store.find(where,select).exec(function(err,result){
        if(err){
            err.status=500;
            next(err);
        } else {
            res.jsonp({result:result});
        }
    });

};

//Create
exports.add=function(req,res,next){
    var store=new Store();
    store.store_name_kr=req.body.store_name_kr;
    store.store_name_en=req.body.store_name_en;
    store.address=req.body.address;
    store.categori=req.body.categori;
    store.location=req.body.location;
    store.regNum=req.body.regNum;
    store.menus=req.body.menus;

    //save method를 사용해 게시물을 DB에 추가한다
    store.save(function(err,result){
        if(err){
            err.status=500;
            next(err);
        } else{
            res.jsonp({result:'success'});
        }
    });

};

//Remove
exports.delete=function(req,res,next){
    console.log(req.params)
    Store.findByIdAndRemove(req.params.store_id).exec(function(err,result){
        if(err){
            err.status=500;
            next(err);
        }else{
            res.jsonp(result);
        }
    });
};

//Update
exports.update=function(req, res,next){//해당하는 아이디를 가진 데이터를 업데이트 시킨다.
    Store.findByIdAndUpdate(req.params.store_id, {
        name_kr:req.body.name_kr,
        name_en:req.body.name_en,
        location:req.body.location,
        menu:req.body.menu,
        categori:req.body.categori
    } ,function(err, updateResult){//업데이트가 완료되었을때의 콜백메소드를 정의한다.
        if(err){
            err.status=500;
            next(err);
        }else{
            res.jsonp({result:"success"});
        }
    });
};

exports.get=function(req,res,next){
    Store.findById(req.params.store_id).exec(function(err,result){
        if(err){
            err.status=500;
            next(err);
        }else{
            res.jsonp(result);
        }
    });
}

//메뉴 추가하는 기능
exports.addMenu=function(req,res,next){

};



