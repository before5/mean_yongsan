/**
 * Created by Bitcamp on 2016-06-09.
 */


'use strict'

var mongoose=require('mongoose');
var connection=require('../../config/mongoose');
var Schema=mongoose.Schema;

var MemberSchema = new Schema({
    userId:{
        type:String,
        default:'',
        trim:true
    },

    userPwd:{
        type:String,
        default:'',
        trim:true
    },

    name:{
        type:String,
        default:'',
        trim:true
    },

    userEmail:{
        type:String,
        default:'',
        trim:true
    },


    regData:{
        type:Date,
        default:Date.now,
        trim:true

    }
});

var Member=connection.model('Member',MemberSchema)//스키마를 Store라는 이름으로 등록을 한다.

module.exports=Member;