/**
 * Created by Bitcamp on 2016-06-02.
 */
//자바스크립트 엄격 모드 적용
'use strict';


//사용한 모듈을 로드한다.
//몽고디비랑 연결하고
var mongoose =require('mongoose'),
    connection=require('../../config/mongoose'),
    Schema=mongoose.Schema;

var StoreSchema = new Schema({
    store_name_kr:{
        type:String,
        default:'',
        trim:true
    },

    store_name_en:{
        type:String,
        default:'',
        trim:true
    },

    address:{
        type:String,
        default:'',
        trim:true
    },

    categori:{
        type:String,
        default:'',
        trim:true
    },

    location:{
        type:Object,
        default:'',
        trim:true
    },

    regNum:{
        type:String,
        default:'',
        trim:true
    },

    menus:{
        type:Array,
        default:'',
        trim:true
    }



});


var Store=connection.model('Store',StoreSchema)//스키마를 Store라는 이름으로 등록을 한다.

module.exports=Store;