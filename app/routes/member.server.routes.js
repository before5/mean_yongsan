/**
 * Created by Bitcamp on 2016-06-09.
 */


var Member=require('../controllers/member.server.controller.js');

module.exports=function(app){

    app.route('/api/member')
        .get(Member.list)
        .post(Member.add);

}