// 자바스크립트 '엄격 모드' 적용
'use strict';

// 사용할 모듈을 로드한다.
var Store = require('../../app/controllers/store.server.controller');


module.exports = function(app) {

	app.route('/api/store')
		.get(Store.list)//Get방식으로 /api/store에 들어온 경우 Store의 목록을 출력해준다.
		.post(Store.add)
	// crud board 관련 라우트
	//json방식으로 컨트롤러에서 데이터를 가지고오고 입력할때 사용한다.

	// crud board 관련 라우트 
	app.route('/api/store/:store_id')//이 URL로 들어오게 되면 get과 put방식 delete방식으로 들어올때 다 다르게 처리를 합니다.
		.get(Store.get)
		.put(Store.update)
		.delete(Store.delete);
};