/**
 * Created by Bitcamp on 2016-06-03.
 */
String.prototype.toKorChars = function() {
    var cCho  = [ 'ㄱ', 'ㄲ', 'ㄴ', 'ㄷ', 'ㄸ', 'ㄹ', 'ㅁ', 'ㅂ', 'ㅃ', 'ㅅ', 'ㅆ', 'ㅇ', 'ㅈ', 'ㅉ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ' ],
        cJung = [ 'ㅏ', 'ㅐ', 'ㅑ', 'ㅒ', 'ㅓ', 'ㅔ', 'ㅕ', 'ㅖ', 'ㅗ', 'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ', 'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ', 'ㅡ', 'ㅢ', 'ㅣ' ],
        cJong = [ '', 'ㄱ', 'ㄲ', 'ㄳ', 'ㄴ', 'ㄵ', 'ㄶ', 'ㄷ', 'ㄹ', 'ㄺ', 'ㄻ', 'ㄼ', 'ㄽ', 'ㄾ', 'ㄿ', 'ㅀ', 'ㅁ', 'ㅂ', 'ㅄ', 'ㅅ', 'ㅆ', 'ㅇ', 'ㅈ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ' ],
        cho, jung, jong;


    var cho_en  = [ 'k', 'kk', 'n', 'd', 'tt', 'l', 'm', 'b', 'pp', 's', 'ss', '', 'j', 'jj', 'ch', 'k', 't','p', 'h' ],
        jung_en = ['a','ae','ya','yae','eo','e','yeo','ye','o','wa','wae','oe','yo','u','wo','we','wi','yu','eu','ui','i'],
        jong_en = [ '','g','kk','k','n','n','n','t','l','l','l','l','l','l','l','l','m','p','p','s','ss','ng','j','ch','k','t','p','h'];


    var str = this;
    var cnt = str.length;
    var chars = [];
    var cCode;
    console.log(str)
    if(str.trim()!=''){
        for (var i = 0; i < cnt; i++) {
            cCode = str.charCodeAt(i);

            if (cCode == 32) { continue; }

            // 한글이 아닌 경우
            if (cCode < 0xAC00 || cCode > 0xD7A3) {
                chars.push(str.charAt(i));
                continue;
            }

            cCode  = str.charCodeAt(i) - 0xAC00;

            jong = cCode % 28; // 종성
            jung = ((cCode - jong) / 28 ) % 21 // 중성
            cho  = (((cCode - jong) / 28 ) - jung ) / 21 // 초성

            chars.push(cho_en[cho], jung_en[jung]);
            if (cJong[jong] !== '') { chars.push(jong_en[jong]); }
        }
        var str=chars.join("");
        return str.substring(0,1).toUpperCase() + str.substring(1);
    }

}