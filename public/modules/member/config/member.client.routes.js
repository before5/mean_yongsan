/**
 * Created by Bitcamp on 2016-06-09.
 */


(function(){
    'use strict';

    angular.module('crud')
        .config(function($routeProvider, $locationProvider){
            $routeProvider.when('/member/register',{
                templateUrl: 'modules/member/views/member.create.html',
                controller:'memberCreateController',
                controllerAs:'member'
            })

            $locationProvider.html5Mode(true);
        })
})();