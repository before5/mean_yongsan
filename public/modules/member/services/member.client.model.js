/**
 * Created by Bitcamp on 2016-06-09.
 */


(function(){
    'use strict';

    angular.module('member')
        .service('memberService', ['memberFactory',function(memberFactory){
            this.addMember=addMember;

            function addMember(memberData){
                var member=new memberFactory(memberData);
               return member.$save();
            }




        }])
})()