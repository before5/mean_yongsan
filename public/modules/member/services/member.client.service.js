/**
 * Created by Bitcamp on 2016-06-09.
 */


(function(){
    'use strict';

    angular.module('member')
        .factory('memberFactory',['$resource', function($resource){
            return $resource('api/member/:member_id',{
                member_id:'@_id'
            },{
                update:{
                    method:'PUT'
                },
                get:{
                    isArray:false
                },
                query:{
                    isArray:false
                }

            })
        }])

})();