/**
 * Created by Bitcamp on 2016-06-02.
 */


(function(){
    'use strict';

    angular.module('store')
        .config(function($routeProvider, $locationProvider){
            $routeProvider
                .when('/store/add', {
                    templateUrl: 'modules/store/views/store.client.create.html',
                    controller: 'storeCreateController',
                    controllerAs: 'store'
                })
                .otherwise({
                    redirectTo: '/'
                });
            // 해당 모듈 부분에 HTML5Mode를 지정해주므로 URL에 '#'이 붙어 생기는 문제를 해결하게 된다.
            $locationProvider.html5Mode(true);
        });

})();