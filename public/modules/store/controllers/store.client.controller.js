/**
 * Created by Bitcamp on 2016-06-02.
 */
(function() {
    // 자바스크립트 '엄격 모드' 적용
    'use strict';
    // crud 모듈의 controller를 구현한다.
    // controller는 어플리케이션의 비즈니스 로직을 구현하기 위해 모델 내 수행되는 기능들을 정의하는 곳
    angular.module('store')

        // 컨트롤러는 아래와 같이 여러개를 다른 이름으로 정의 가능하다.
        .controller('storeController', ['$scope', 'storeService', StoreCtrl])
        .controller('storeCreateController', ['$scope', 'storeService', '$location','$http',  StoreCreateCtrl])
        .directive('krInput', function ($parse) {

        return {
            priority: 2,
            restrict: 'A',
            compile: function (element) {
                element.on('compositionstart', function (e) {
                    e.stopImmediatePropagation();
                });
            }
        }
    });
    // 게시판 목록 컨트롤러
    function StoreCtrl($scope, storeService) {

    }

    //게시글 작성 컨트롤러
    function StoreCreateCtrl($scope, storeService ,$location, $http) {
        $scope.storeData={
            "categori":"한식"
        };
        $scope.storeData.location={
            "lat":37.5615682,
            "lng":126.9733249
        }

        if (navigator.geolocation) {//현재 위치 정보를 가지고 오는것이 활성화되어 있으면,
            // GeoLocation을 이용해서 접속 위치를 얻어옵니다
            navigator.geolocation.getCurrentPosition(function(position) {
                $scope.storeData.location.lat = position.coords.latitude; // 위도
                $scope.storeData.location.lng = position.coords.longitude; // 경도
                WGS84toString($http, $scope.storeData.location.lat,  $scope.storeData.location.lng,function(result){
                    $scope.storeData.address=result;
                });
            });
        }else{
            WGS84toString($http, $scope.storeData.location.lat,  $scope.storeData.location.lng,function(result){
                $scope.storeData.address=result;
            });

            $scope.lat=lat;
            $scope.lng=lng;
        }


        //Categoris
        $scope.categori_options={
            "default_value": "한식",
            "values": [ "한식", "중식", "분식", "일식"]
        }


        $scope.setEngStoreName=function(){
            console.log("키프레스")
            if($scope.storeData.store_name_kr!=null){
                $scope.storeData.store_name_en=$scope.storeData.store_name_kr.toKorChars();
            }else{
                $scope.storeData.store_name_en="";
            }
        }


        $scope.setEngMenuName=function(){
            if($scope.menu_name_kr!=null){
                $scope.menu_name_en=$scope.menu_name_kr.toKorChars();
            }else{
                $scope.menu_name_en="";
            }
        }

        $scope.storeData.menus=[];
        $scope.addMenu=function(){
            $scope.storeData.menus.push({"menu_name_kr":$scope.menu_name_kr,"menu_name_en":$scope.menu_name_en, "price":$scope.price});
            console.log($scope.storeData);
        }

        $scope.removeMenu=function(){
            $scope.storeData.menus.splice($scope.selectNum,1);
        }

        $scope.selectMenu=function(index){
            $scope.selectNum=index;

        }

        $scope.saveStore = function() {
            // 주입 받은 crudService를 통해 addCrud()를 호출한다.
            // addCrud()는 게시판 등록을 수행하는 메소드이다.

            storeService.addStore($scope.storeData).then( function(data) {
                if(data.result == 'success') {
                    // 작성 완료 후, 다시 목록으로 이동
                    $location.path('/');
                }
            }, function(error) {
                // 통신 오류가 발생하였을 때 처리
                console.log('HTTP 통신 에러');
            });
        }

    }


    function WGS84toString(http, lat, lng, callback){
        http({
            method : "GET",
            url : "http://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lng+"&language=ko"
        }).then(function mySucces(response) {
            callback(response.data.results[0].formatted_address);
        }, function myError(response) {
            console.log(response);
        });
    }
})();