/**
 * Created by Bitcamp on 2016-06-02.
 */

(function() {
    // 자바스크립트 '엄격 모드' 적용
    'use strict';

    // 새로운 모듈을 주 모듈에 등록한다.
    ApplicationConfiguration.registerModule('store');
})();